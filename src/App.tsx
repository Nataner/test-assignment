import { QuartalBoard } from './components/Quartal-board/Quartal-board';
import { Table } from './components/Table/Table';
import { OpenDialogButton } from './components/Buttons/Open-dialog/Open-dialog-button';
import { CreateTaskDialog } from './components/Dialogs/Create-dialog/Create-dialog';
import { UpdateTaskDialog } from './components/Dialogs/Update-dialog/Update-dialog';
import { closeModal } from './redux/Slices/Modals';
import './App.scss';

const App: React.FC = () => {

    return (
        <div className="App">
            <div className='roof-top'>
                <OpenDialogButton 
                    type='create' 
                    element={ <CreateTaskDialog closeModal={closeModal()} /> } 
                    text='Create task'
                />
                <OpenDialogButton 
                    type='update'
                    element={ <UpdateTaskDialog closeModal={closeModal()} /> } 
                    text='Update task' 
                />
            </div>
            <QuartalBoard/>
            <Table/>
        </div>
    );
}

export default App
