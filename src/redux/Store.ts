import { configureStore } from "@reduxjs/toolkit";
import { quartalSlice } from "./Slices/Quartal";
import { tasksSlice } from "./Slices/Tasks";
import { modalsSlice } from "./Slices/Modals";

export const Store = configureStore({
    reducer: {
        quartal: quartalSlice.reducer,
        tasks: tasksSlice.reducer,
        modal: modalsSlice.reducer
    }
});


export type StoreState = ReturnType< typeof Store.getState >;

export type StoreDispatch = typeof Store.dispatch;