import { createSlice } from "@reduxjs/toolkit";


export const modalsSlice = createSlice({
    name: 'modals',
    initialState: {
        modalType: '',
        isVisible: false
    },
    reducers: {
        openModal: (state, action) => {
            return {
                modalType: action.payload,
                isVisible: true
            }
        },
        closeModal: (state) => {
            return {
                ...state,
                isVisible: false
            }
        }
    }
})

export const { openModal, closeModal } = modalsSlice.actions;