import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const tasksInitialState = [
  {
    id: 1,
    name: "Task 1",
    startDate: new Date(2022, 4, 5).setHours(0, 0, 0, 0),
    endDate: new Date(2022, 4, 25).setHours(0, 0, 0, 0),
  },
  {
    id: 2,
    name: "Task 2",
    startDate: new Date(2023, 2, 1).setHours(0, 0, 0, 0),
    endDate: new Date(2023, 2, 9).setHours(0, 0, 0, 0),
  },
];

export const tasksSlice = createSlice({
  name: "tasks",
  initialState: tasksInitialState,
  reducers: {

    addTask: (tasks, action) => {
      return [...tasks, action.payload];
    },

    addMultipleTasks: (tasks) => {
      return [...tasks];
    },

    updateMultipleTasks: (tasks) => {
      return [...tasks];
    },

    updateTask: {
      reducer: (state, action: PayloadAction<any>) => {
        return [...state].map((value) => {
          if(value.id === action.payload.id) {
            return action.payload
          }
          return value
        });
      },
      /* All possible payload's preparations should be done in prepare function */
      prepare: (value: any) => {
        return {
          payload: {
            ...value,
            startDate: new Date(value.startDate).setHours(0, 0, 0, 0),
            endDate: new Date(value.endDate).setHours(0, 0, 0, 0),
          },
        };
      },
    },

  },
});

export const { addTask, addMultipleTasks, updateTask, updateMultipleTasks } =
  tasksSlice.actions;
