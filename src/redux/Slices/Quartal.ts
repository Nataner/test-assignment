import { createSlice } from '@reduxjs/toolkit';
import { GetQuartalNumber } from '../../utils/quartal/getQuartalNumber';
import { Quartal } from '../../types/Quartals';

const currentYear = new Date().getFullYear();
/*- for * 3 logic is that every 3rd month of current quartal number is last month -*/
const initialLastQuartalMonth = GetQuartalNumber(new Date().getMonth()) * 3;

/*- minus 2 months from last quartal's month equals this quartal's start month, minus 3 here for JS month's indexing, that starts from zero -*/
const initialStartQuartalMonthNumber = initialLastQuartalMonth - 3;
/*- minus 1 for JS month's indexing -*/
const initialEndQuartalMonthNumber = initialLastQuartalMonth - 1;

const initialStartQuartalTime =  new Date(currentYear, initialStartQuartalMonthNumber, 1).getTime();

const initialLastQuartalTime = new Date(currentYear, initialEndQuartalMonthNumber, 1).getTime();

const quartalInitialState: Quartal = {
    quartalNumber: GetQuartalNumber(new Date().getMonth()),
    startMonthNumber: initialStartQuartalMonthNumber,
    endMonthNumber: initialEndQuartalMonthNumber,
    startQuartalTime: initialStartQuartalTime,
    endQuartalTime: initialLastQuartalTime,
    year: currentYear
}

export const quartalSlice = createSlice({
    name: 'quartal',
    initialState: quartalInitialState,
    reducers: {
        toNextQuartal: (state: Quartal) => {
            const { startMonthNumber, endMonthNumber, quartalNumber, year } = state;
            const isLastYearQuartal: boolean = quartalNumber === 4;
            const futureYear: number = isLastYearQuartal ? year + 1 : year;
            const futureStartMonth: number = isLastYearQuartal ? 0 : startMonthNumber + 3;
            const futureLastMonth: number = isLastYearQuartal ? 2 : endMonthNumber + 3;

            return {
                quartalNumber: isLastYearQuartal ? 1 : quartalNumber + 1,
                startMonthNumber: futureStartMonth,
                startQuartalTime: new Date(futureYear, futureStartMonth, 1).getTime(),
                endMonthNumber: futureLastMonth,
                endQuartalTime: new Date(futureYear, futureLastMonth, 1).getTime(),
                year: futureYear
            }
        },
        toPreviousQuartal: (state: Quartal) => {
            const { startMonthNumber, endMonthNumber, quartalNumber, year } = state;
            const isFirstYearQuartal: boolean = quartalNumber === 1;
            const pastYear: number = isFirstYearQuartal ? year - 1 : year;
            const pastStartMonth: number = isFirstYearQuartal ? 9 : startMonthNumber - 3;
            const pastLastMonth: number = isFirstYearQuartal ? 11 : endMonthNumber - 3;

            return {
                quartalNumber: isFirstYearQuartal? 4 : quartalNumber - 1,
                startMonthNumber: pastStartMonth,
                startQuartalTime: new Date(pastYear, pastStartMonth, 1).getTime(),
                endMonthNumber: pastLastMonth,
                endQuartalTime: new Date(pastYear, pastLastMonth, 1).getTime(),
                year: pastYear
            }
        }
    }
});

export const { toNextQuartal, toPreviousQuartal } = quartalSlice.actions;