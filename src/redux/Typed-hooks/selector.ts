import { TypedUseSelectorHook, useSelector } from 'react-redux';
import type { StoreState } from '../Store';

export const useStoreSelector: TypedUseSelectorHook<StoreState> = useSelector;