import { useDispatch } from "react-redux";
import { StoreDispatch } from "../Store";

type DispatchFunc = () => StoreDispatch;
export const useStoreDispatch: DispatchFunc = useDispatch;