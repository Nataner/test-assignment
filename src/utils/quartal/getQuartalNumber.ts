import type { YearMonths } from "../../types/Quartals"

export const GetQuartalNumber = (month: number | YearMonths): number => {
    if(typeof month === 'string') {
        month = new Date(`${month} 1,${new Date().getFullYear()}`).getMonth();
    }
    if(0 >= month || month <= 2) return 1;
    if(3 >= month || month <= 5) return 2;
    if(6 >= month || month <= 8) return 3;
    return 4;
}