import moment from "moment";
//Values are constant exept first quartal, that depends on leap year.
export const getQuartalDaysNumber = (quartalNumber: number, year: number): number => {

    if(quartalNumber === 1) {
        const isLeapYear = moment(new Date(year, 1, 1)).daysInMonth() === 29;
        return isLeapYear ? 91 : 90;
    }

    if(quartalNumber === 2) {
        return 91;
    }
    
    return 92;
}