export interface Quartal {
    quartalNumber: number;
    startMonthNumber: number;
    startQuartalTime: number;
    endMonthNumber: number;
    endQuartalTime: number;
    year: number;
}

export type YearMonths = 'January' | 'February' | 'March' | 'April' | 'May' | 'June' | 'July' | 'August' | 'September' | 'October' | 'November' | 'December';