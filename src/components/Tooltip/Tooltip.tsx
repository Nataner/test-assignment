import './Tooltip.scss';

type TooltipProps = {
  task: any;
  top: number;
}

export const Tooltip = ({ task, top }: TooltipProps): JSX.Element => {
  /* REDUX  */
  /* REACT  */
  /* UI-LOGIC  */
  return (
    <div
      style={{ top: `${top}px` }}
      className='tooltip'
    >
      <h3 className='tooltip-task-name'>{ task.name }</h3>
      <p className='tooltip-dates'>{ new Date(task.startDate).toLocaleDateString().substring(0, 10) } - { new Date(task.endDate).toLocaleDateString().substring(0, 10) }</p>
      <div className='tooltip-arrow'></div>
    </div>
  )
}