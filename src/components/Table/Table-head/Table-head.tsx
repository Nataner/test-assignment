import { QuartalMonths } from './Table-quartal-months/Table-quartal-months';
import { QuartalWeeks } from "./Table-quartal-weeks/Quartal-weeks";

export const TableHead = (): JSX.Element => {
  /* REDUX  */
  /* REACT  */
  /* UI-LOGIC  */

    return (
        <thead>
            <QuartalMonths/>
            <QuartalWeeks/>
        </thead>
    )
}