import { useState, useEffect } from 'react';

import { MonthList } from './Months-list/Month-list';
import { SkeletonCell } from '../../../../components/Skeleton/Cell/Skeleton-cell';
import { useStoreSelector } from '../../../../redux/Typed-hooks/Selector';
import type { YearMonths } from '../../../../types/Quartals';
import './Table-quartal-months.scss';
import moment from 'moment';

const months = moment.months() as YearMonths[];

export const QuartalMonths = (): JSX.Element => {

    const [quartalMonths, setQuartalMonths] = useState<YearMonths[]>([]);
    const skeletonMonths = [1,2,3].map((el) => <SkeletonCell key={el} />);
    const quartal = useStoreSelector((state) => state.quartal);
    
    useEffect(() => {
        const filterFunction = (_: string, index: number): boolean => (
            quartal.startMonthNumber <= index && index <= quartal.endMonthNumber    
        );
        setQuartalMonths(months.filter(filterFunction))
    }, [quartal]);

    return (
        <tr className='quartal-view'>
            { quartalMonths.length ? <MonthList months={quartalMonths} /> : skeletonMonths }
        </tr>
    );
}