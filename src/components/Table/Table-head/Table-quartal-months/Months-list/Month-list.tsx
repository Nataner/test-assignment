import { YearMonths } from "../../../../../types/Quartals";
import './Month-list.scss';

interface QuartalMonthsProps {
    months: YearMonths[];
} 

export const MonthList = ({ months }: QuartalMonthsProps): JSX.Element => {
    return <> 
        {
            months.map( (month) => <th className='month-cell' key={month}> { month } </th> )
        }
    </>
}