import './Quartal-weeks.scss';

export const QuartalWeeks = (): JSX.Element => {
  /* REDUX  */
  /* REACT  */
  /* UI-LOGIC  */
    
    const weeks = [1,2,3,4,5,6,7,8,9,10,11,12];
    
    return (
        <tr className='weeks'>
            {
                weeks.map((week) => <th key={week} className='week'> { week } </th> )
            }
        </tr>
    );
}