import { TableHead } from "./Table-head/Table-head";
import { TableBody } from "./Table-body/Table-body";
import "./Table.scss";

export const Table: React.FC = () => {
  /* REDUX  */
  /* REACT  */
  /* UI-LOGIC  */

  return (
    <table>
      <TableHead />
      <TableBody />
    </table>
  );
};
