import { useStoreSelector } from "../../../redux/Typed-hooks/Selector";
import { TaskList } from "./Task-list/Task-list";
import { getQuartalDaysNumber } from "../../../utils/quartal/getQuartalDaysNumber";

export const TableBody = (): JSX.Element => {

    /* REDUX  */
    
    const tasks = useStoreSelector((store) => store.tasks);
    const quartal = useStoreSelector((store) => store.quartal);
    
    /* REACT  */
    /* UI-LOGIC  */
    const currentQuartalDaysNumber = getQuartalDaysNumber(quartal.quartalNumber, quartal.year);

    return (
        <tbody>
            <TaskList tasks={tasks} quartal={quartal} currentQuartalDaysNumber={currentQuartalDaysNumber} />
        </tbody>
    )
}