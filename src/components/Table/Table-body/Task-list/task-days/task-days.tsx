interface TaskDaysProps {
  quartalDays: string[];
  task: any;
}

export const TaskDays = ({ quartalDays, task }: TaskDaysProps): JSX.Element => {

  const setIfScheduled = (day: string): boolean => task.startDate <= day && day <= task.endDate;

  return (
    <>
      {quartalDays.map((day) => (
        <th
          key={`${task.id}-${day}`}
          className={`day ${ setIfScheduled(day) ? "scheduled" : ""}`}
        ></th>
      ))}
    </>
  );
};
