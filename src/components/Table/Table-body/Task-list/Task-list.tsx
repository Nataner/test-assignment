import { TaskDays } from "./Task-days/Task-days";
import { Quartal } from "../../../../types/Quartals";
import React, { useState } from "react";
import { Tooltip } from "../../../Tooltip/Tooltip";
import { createPortal } from "react-dom";
import "./Task-list.scss";

interface TaskListProps {
  tasks: any[];
  quartal: any;
  currentQuartalDaysNumber: number;
}

export const TaskList = ({ tasks, quartal, currentQuartalDaysNumber }: TaskListProps): JSX.Element => {

  /* REDUX  */
  /* REACT  */

  const [ hoveredOn, setHoveredOn ] = useState<number | null>(null);
  const [ hoveredY, setHoveredY ] = useState<number>(0);

  /* UI-LOGIC  */

  /* Every day, in row of quartal, required his own key, so date would be a good and semantic-understandable key */
  const calculateDates = (daysNumber: number, quartal: Quartal): any[] => {
    const result = [];
    while (daysNumber > 1) {
      result.push( 
        new Date(quartal.year, quartal.startMonthNumber, daysNumber).setHours(0, 0, 0, 0)
      );
      daysNumber--;
    }
    return result.reverse();
  };

  const quartalDays = calculateDates(currentQuartalDaysNumber, quartal);

  const mouseHoverHandler = (event: React.MouseEvent<HTMLTableRowElement>, task: any) => {
    const target = event.currentTarget;
    setHoveredOn(task.id);
    setHoveredY(target.offsetTop + target.offsetHeight);
  };

  const mouseLeaverHandler = () => {
    setHoveredOn(null);
    setHoveredY(0);
  }

  return (
    <>
      {tasks.map((task) => (
        <tr
          key={task.id}
          className="task-row"
          onMouseEnter={(event) => mouseHoverHandler(event, task)}
          onMouseLeave={mouseLeaverHandler}
        >
          <TaskDays task={task} quartalDays={quartalDays} />
          { 
            hoveredOn === task.id && createPortal(
              <Tooltip top={hoveredY} task={task} />, document.getElementById("root") ?? document.body
            )
          }
        </tr>
      ))}
    </>
  );
};
