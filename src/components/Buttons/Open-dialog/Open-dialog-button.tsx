import { createPortal } from 'react-dom';
import { useStoreDispatch } from '../../../redux/Typed-hooks/Dispatch';
import { useStoreSelector } from '../../../redux/Typed-hooks/Selector';
import { openModal } from '../../../redux/Slices/Modals';
import './Open-dialog-button.scss';

interface OpenDialogProps {
    text: string;
    type: string;
    element: React.ReactNode;
}

export const OpenDialogButton = ({ text, element, type }: OpenDialogProps): JSX.Element => {

    const modalState = useStoreSelector((store) => store.modal);
    const dispatch = useStoreDispatch();

    const open = (): void => {
        dispatch(openModal(type))
    }

    return (
        <>
        <button onClick={open} className="button">
            { text }
        </button>

        {
            (modalState.isVisible && modalState.modalType === type) && createPortal(element, document.body)
        }
        </>
    );
}