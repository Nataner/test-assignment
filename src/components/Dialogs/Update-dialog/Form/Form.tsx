import { useEffect, useState, FormEvent } from "react";
import { useStoreDispatch } from "../../../../redux/Typed-hooks/Dispatch";
import { ReduxAction } from "../../../../types/Actions";
import { useStoreSelector } from "../../../../redux/Typed-hooks/Selector";
import { updateTask } from "../../../../redux/Slices/Tasks";

type FormProps = {
  selectedTask: any;
  dropdown: any;
  closeModal: ReduxAction;
};

export const Form = ({
  selectedTask,
  dropdown,
  closeModal
}: FormProps) => {

  const dispatch = useStoreDispatch();
  const tasks = useStoreSelector((store) => store.tasks);

  const [ taskToUpdate, setTaskToUpdate ] = useState<any>(selectedTask);
  
  /* This is reqiured to update state when user reselects after starting to change task */
  useEffect(() => setTaskToUpdate(selectedTask), [selectedTask])

  const handleUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
    const id = event.target.id;
    const value = event.target.value;

    const dictionary = {
      "task-name": () => setTaskToUpdate({...taskToUpdate, name: value}),
      "start-date": () => setTaskToUpdate({...taskToUpdate, startDate: value}),
      "end-date": () => setTaskToUpdate({...taskToUpdate, endDate: value}),
    }[id];

    dictionary && dictionary();
  };
  
  const update = (event: FormEvent) => {
    event.preventDefault()

    dispatch(
      updateTask({...taskToUpdate})
    );
    dispatch(closeModal);
  }

  return (
    <form className="create-form" onSubmit={update}>
      {dropdown}

      <label htmlFor="task-name">Task name: </label>
      <input
        onChange={(event) => handleUpdate(event)}
        value={taskToUpdate.name}
        type="text"
        id="task-name"
      />

      <label htmlFor="start-date">Start Date: </label>
      <input
        onChange={(event) => handleUpdate(event)}
        value={new Date(taskToUpdate.startDate).toISOString().substring(0, 10)}
        type="date"
        id="start-date"
      />

      <label htmlFor="end-date">End Date: </label>
      <input
        onChange={(event) => handleUpdate(event)}
        value={new Date(taskToUpdate.endDate).toISOString().substring(0, 10)}
        min={new Date(taskToUpdate.startDate).toISOString().substring(0, 10)}
        type="date"
        id="end-date"
      />

      <button type="submit">Submit</button>
    </form>
  );
};
