import { useEffect, useState } from "react";
import { useStoreDispatch } from "../../../redux/Typed-hooks/Dispatch";
import { ReduxAction } from "../../../types/Actions";
import { Dropdown } from "../../Dropdown/Dropdown";
import "./Update-dialog.scss";
import { useStoreSelector } from "../../../redux/Typed-hooks/Selector";
import { Form } from "./Form/Form";

interface UpdateTaskDialogProps {
  closeModal: ReduxAction;
}

export const UpdateTaskDialog = ({
  closeModal,
}: UpdateTaskDialogProps): JSX.Element => {

  const tasks = useStoreSelector((store) => store.tasks);

  const [selectedTask, setSelectedTask] = useState<any>();

  const dispatch = useStoreDispatch();
  const close = (event: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
    if (event.target !== event.currentTarget) return;
    dispatch(closeModal);
  };

  useEffect(() => {}, [selectedTask])

  const selectTaskComponent = (
    <Dropdown
      selectTask={setSelectedTask}
      selectedTask={selectedTask}
      options={tasks}
    />
  );

  const formComponent = (
    <Form 
      selectedTask={selectedTask}
      dropdown={selectTaskComponent}
      closeModal={closeModal}
    />
  );

  return (
    <div className="modal-overlay" onClick={close}>
      <div className="modal">
        {selectedTask ? formComponent : selectTaskComponent}
      </div>
    </div>
  );
};
