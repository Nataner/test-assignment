import { FormEvent, useState } from "react";
import { useStoreDispatch } from "../../../redux/Typed-hooks/Dispatch";
import { ReduxAction } from "../../../types/Actions";
import "./Create-dialog.scss";
import { addTask } from "../../../redux/Slices/Tasks";

interface CreateTaskDialogProps {
  closeModal: ReduxAction;
}

export const CreateTaskDialog = ({
  closeModal,
}: CreateTaskDialogProps): JSX.Element => {
  const [taskName, setTaskName] = useState("");
  const [taskStartDate, setTaskStartDate] = useState("");
  const [taskEndDate, setTaskEndDate] = useState("");

  const dispatch = useStoreDispatch();

  const close = (event: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
    if (event.target !== event.currentTarget) return;
    dispatch(closeModal);
  };

  const saveTaskData = (event: FormEvent<HTMLInputElement>) => {
    const value = (event.target as HTMLInputElement).value;
    const id: string = (event.target as HTMLInputElement).id;

    const dictionary = {
      "task-name": () => setTaskName(value),
      "start-date": () => setTaskStartDate(value),
      "end-date": () => setTaskEndDate(value),
    }[id];

    dictionary && dictionary();
  };

  const createTask = (event: FormEvent) => {
    event.preventDefault();

    dispatch(
      addTask({
        id: Math.random(),
        name: taskName,
        startDate: new Date(taskStartDate).setHours(0, 0, 0, 0),
        endDate: new Date(taskEndDate).setHours(0, 0, 0, 0),
      })
    );
    dispatch(closeModal);
  };

  return (
    <div className="modal-overlay" onClick={close}>
      <div className="modal">
        <form onSubmit={createTask} className="create-form">
          <label htmlFor="task-name">Task name: </label>
          <input type="text" id="task-name" onInput={saveTaskData} />
          <label htmlFor="start-date">Start Date: </label>
          <input type="date" id="start-date" onInput={saveTaskData} />
          <label htmlFor="end-date">End Date: </label>
          <input type="date" id="end-date" onInput={saveTaskData} />

          <button type="submit">Submit</button>
        </form>
      </div>
    </div>
  );
};
