import { useState } from "react";
import "./Dropdown.scss";

type DropDownProps = {
  options: any;
  selectTask: any;
  selectedTask: any;
};

export const Dropdown = ({
  options,
  selectTask,
  selectedTask
}: DropDownProps): JSX.Element => {
  const [isOpened, setIsOpened] = useState<boolean>(false);

  const toggleDropDown = () => setIsOpened(!isOpened);

  const handleItemClick = (newTask: any) => {
    selectTask(() => newTask);
    toggleDropDown();
  };

  return (
    <div className="dropdown-container">
      <div className="dropdown-header" onClick={toggleDropDown}>
        { selectedTask ? selectedTask.name : "Select task..." }
        <i className={`fa fa-chevron-right icon ${isOpened && "open"}`}></i>
      </div>
      <div className={`dropdown-body ${isOpened && "open"}`}>
        {options.map((item: any) => (
          <div
            key={item.id}
            className="dropdown-item"
            onClick={() => handleItemClick(item)}
          >
            {item.name}
          </div>
        ))}
      </div>
    </div>
  );
};
