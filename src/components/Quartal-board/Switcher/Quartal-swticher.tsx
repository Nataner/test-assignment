import { BsArrowLeftCircle, BsArrowRightCircle } from 'react-icons/bs';

import { toNextQuartal, toPreviousQuartal } from '../../../redux/Slices/Quartal';
import { useStoreDispatch } from '../../../redux/Typed-hooks/Dispatch';
import './Quartal-switcher.scss';

type SwitcherType = 'Next' | 'Previous';
interface TableSwitcherComponent {
    type: SwitcherType;
}

export const TableQuartalSwitcher = ({ type }: TableSwitcherComponent): JSX.Element => {

    const dispatch = useStoreDispatch();

    const switchQuartal = (): void => {
        type === 'Next' ? dispatch(toNextQuartal()) : dispatch(toPreviousQuartal());
    }

    return (
        <div className='switcher-container'>
            { type === 'Next' ? <BsArrowRightCircle className='switcher' onClick={switchQuartal} /> : <BsArrowLeftCircle className='switcher' onClick={switchQuartal} /> }
        </div>
    )
}