import { useStoreSelector } from '../../redux/Typed-hooks/Selector';
import { TableQuartalSwitcher } from './Switcher/Quartal-swticher';
import './Quartal-board.scss';

export const QuartalBoard = (): JSX.Element => {

    const quartal = useStoreSelector((state) => state.quartal);

    return (
        <div className='board'>
            <TableQuartalSwitcher type='Previous'/>
                <div className='quartal-board'>
                    <p>Currenct quartal: { quartal.quartalNumber }</p>
                    <p>Year: { quartal.year }</p>
                </div>
            <TableQuartalSwitcher type='Next'/>
        </div>
    );
}