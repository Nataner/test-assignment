import './Skeleton-cell.scss';

export const SkeletonCell = (): JSX.Element => {

    return (
        <th className='month-skeleton'></th>
    );
}