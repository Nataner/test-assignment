import './Skeleton-board.scss';

export const SkeletonBoard = (): JSX.Element => {
    return (
        <div className='skeleton-board'></div>
    )
}